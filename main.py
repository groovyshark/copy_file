from lxml import etree
from shutil import copyfile
import argparse as ap


def perror(error_string):
    print(error_string)
    exit(1)


def main():
    parser = ap.ArgumentParser()
    parser.add_argument("-n", "--name", default="config.xml")
    args = parser.parse_args()

    try:
        tree = etree.parse(args.name)
    except etree.XMLSyntaxError:
        perror("File not valid")
    except OSError:
        perror("Can't read file")

    nodes = tree.xpath("/config/filearray/file")
    for node in nodes:
        name = node.get("name")
        src = node.get("srcpath") + "/" + name
        dst = node.get("dstpath") + "/" + name

        try:
            copyfile(src, dst)
            print("copy complete:", src, "->", dst)
        except OSError:
            print("Can't copy file:", src)


if __name__ == "__main__":
    main()
